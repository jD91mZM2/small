# small

This is a library that provides "small" types (meaning things are allocated on the stack up to a certain size).

Currently there is only an implementation of a small string.
